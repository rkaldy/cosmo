import os
from flask import jsonify, request, Blueprint, abort, make_response
from flask_login import login_required
from . import db
from .model import Astronaut


bp = Blueprint("rest", __name__, url_prefix="/api/astronaut")

@bp.route("/", methods=["GET"])
@login_required
def list():
    cursor = Astronaut.query.order_by(Astronaut.surname, Astronaut.name)
    if "page" in request.args:
        limit = int(request.args["pageSize"])
        offset = (int(request.args["page"]) - 1) * limit
        cursor = cursor.limit(limit).offset(offset)
    astronauts = cursor.all()
    return jsonify({
        "itemsCount": Astronaut.query.count(),
        "data": [ast.to_json() for ast in astronauts]
    })

@bp.route("/<int:id>", methods=["GET"])
@login_required
def get(id):
    astronaut = Astronaut.query.get_or_404(id)
    return jsonify(astronaut.to_json())

@bp.route("/", methods=["POST"])
@login_required
def create():
    if not request.json:
        abort(400)
    astronaut = Astronaut()
    astronaut.from_json(request.json)
    db.session.add(astronaut)
    db.session.commit()
    return jsonify(astronaut.to_json())

@bp.route("/<int:id>", methods=["PUT"])
@login_required
def update(id):
    if not request.json:
        abort(400)
    astronaut = Astronaut.query.get_or_404(id)
    astronaut.from_json(request.json)
    db.session.commit()
    return jsonify(astronaut.to_json())

@bp.route("/<int:id>", methods=["DELETE"])
@login_required
def delete(id):
    astronaut = Astronaut.query.get_or_404(id)
    db.session.delete(astronaut)
    db.session.commit()
    return jsonify(astronaut.to_json())

@bp.route("/import", methods=["POST"])
@login_required
def xmlimport():
    if "file" not in request.files:
        abort(make_response(jsonify(error="No import file sent"), 400))
    try:
        rowsImported = Astronaut.xml_import(request.files["file"].stream, "clean" in request.args)
        return jsonify(rowsImported=rowsImported)
    except Exception as e:
        abort(make_response(jsonify(error=str(e)), 500))
