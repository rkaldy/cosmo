import json
from flask import url_for, current_app, redirect, request, abort, render_template, Blueprint
from flask_login import login_user, logout_user, current_user, login_required
from . import oauth, login_manager
from .user import User


def decode_json(payload):
    return json.loads(payload.decode("utf-8"))

def redirect_uri():
    if "ROOT_URL" in current_app.config and current_app.config["ROOT_URL"] != None:
        return current_app.config["ROOT_URL"] + url_for("auth.callback")
    else:
        return url_for("auth.callback", _external=True)


bp = Blueprint("auth", __name__, url_prefix="/auth")

@login_manager.unauthorized_handler
def authorize():
    if request.blueprint == "rest":
        abort(401)
    else:
        return redirect(oauth.get_authorize_url(
            scope="identity",
            response_type="code",
            redirect_uri=redirect_uri()
        ))

@bp.route("/callback")
def callback():
    if "error" in request.args:
        abort(403, request.args["error_description"])
    elif "error_description" in request.args:
        abort(403, request.args["error_description"])
    elif "code" not in request.args:
        abort(500, "No ticket provided")

    try:
        session = oauth.get_auth_session(
            data={
                "grant_type": "authorization_code",
                "code": request.args["code"],
                "redirect_uri": redirect_uri()
            },
            decoder=decode_json
        )
        ret = session.get("user").json()
        if ret["firstname"] == "":
            username = ret["username"]
        else:
            username = ret["firstname"] + " " + ret["lastname"]
        login_user(User(username))
        return redirect(url_for("main.index"))
        
    except KeyError as e:
        abort(403, str(e))

@bp.route("/logout")
@login_required
def logout():
    logout_user()
    return render_template("logout.html")
