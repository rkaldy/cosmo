from flask import Blueprint, render_template, current_app
from flask_login import login_required


bp = Blueprint("main", __name__, url_prefix="/")

@bp.route("/")
@login_required
def index():
    return render_template("index.html")

@bp.route("/import")
@login_required
def xmlimport():
    return render_template("import.html")

