ajaxErrorHandler = function(jqXHR) {
    if (jqXHR.status == 401) {
		window.location.redirect('http://seznam.cz'); 
	} else {
		alert('HTTP error, code=' + jqXHR.status);
    }
}

dateValidator = {
    validator: "pattern",
    param: "[0-9]{4}-[0-9]{2}-[0-9]{2}",
    message: function() {
        return "Invalid date";
    }
};


resUrl = "api/astronaut/";
$(function() {
    $("#grid").jsGrid({
		width: "100%",

        autoload: true,
        inserting: true,
        editing: true,
        confirmDeleting: false,
		paging: true,
		pageLoading: true,
        pageSize: 15,
        pageButtonCount: 5,

		fields: [
			{ name: "name", title: "Name", type: "text", validate: "required" },
			{ name: "surname", title: "Surname", type: "text", validate: "required" },
			{ name: "birth_date", title: "Birth date", type: "text", validate: dateValidator },
			{ name: "nationality", title: "Nationality", type: "text" },
			{ type: "control" }
		],
        
		controller: {
            loadData: function(filter) {
				var url = resUrl;
                if ("pageIndex" in filter && "pageSize" in filter) {
                    url += "?page=" + filter.pageIndex + "&pageSize=" + filter.pageSize;
                }
                return $.ajax({
                    type: "GET",
                    url: url,
                    error: ajaxErrorHandler
                });            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: resUrl,
                    data: JSON.stringify(item),
					contentType: "application/json",
                    error: ajaxErrorHandler
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "PUT",
                    url: resUrl + item.id,
                    data: JSON.stringify(item),
					contentType: "application/json",
                    error: ajaxErrorHandler
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: resUrl + item.id,
                    error: ajaxErrorHandler
                });
            }
        },
    });
});

$("#pager").on("change", function() {
    var page = parseInt($(this).val(), 10);
    $("#jsGrid").jsGrid("openPage", page);
});
