import os
import xml.sax
import time
from . import db
from sqlalchemy.orm import Session
from threading import Thread


class Astronaut(db.Model):
    __tablename__ = "astronaut"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    surname = db.Column(db.String(80), nullable=False)
    birth_date = db.Column(db.Date, nullable=True)
    nationality = db.Column(db.String(40), nullable=True)

    def from_json(self, json):
        self.name = json.get("name")
        self.surname = json.get("surname")
        self.birth_date = json.get("birth_date")
        self.nationality = json.get("nationality")

    def to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "surname": self.surname,
            "birth_date": self.birth_date.strftime("%Y-%m-%d"),
            "nationality": self.nationality
        }

    def xml_import(xmlstream, clean=False):
        if clean:
            print("Clean database")
            Astronaut.query.delete()
            db.session.commit()
        print("XML import start")
        handler = XMLHandler(200000)
        parser = xml.sax.make_parser()
        parser.setContentHandler(handler)
        parser.parse(xmlstream)
        return handler.totalRows


class XMLHandler(xml.sax.ContentHandler):
    COLUMN_ORDER = {"name": 0, "surname": 1, "birth_date": 2, "nationality": 3}

    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.batch = 0
        self.totalRows = 0
        self.storeThread = None
        self.newBatch()
        self.lastTime = time.time()

    def newBatch(self):
        self.batch += 1
        self.count = 0
        self.csv = open(f"/tmp/cosmo_import_{self.batch}.csv", "w")

    def storeBatch(self, csv, session):
        self.totalRows += self.count
        csv.close()
        sql = f"LOAD DATA LOCAL INFILE '{csv.name}' INTO TABLE astronaut FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (name, surname, birth_date, nationality)"
        session.execute(sql)
        session.flush()
        session.close()
        os.remove(csv.name)
        print("Batch %s processed, elapsed time %f s, %d rows imported so far" % (csv.name, time.time() - self.lastTime, self.totalRows))
        self.lastTime = time.time()

    def startElement(self, tag, attrs):
        if tag == "astronaut":
            self.row = [""] * len(self.COLUMN_ORDER)
        elif tag in self.COLUMN_ORDER:
            self.currentColumn = self.COLUMN_ORDER[tag]
        else:
            self.currentColumn = None

    def endElement(self, tag):
        self.currentColumn = None
        if tag == "astronaut":
            self.csv.write(",".join(self.row))
            self.csv.write("\n")
            self.count += 1
            if self.count == self.batch_size:
                if self.storeThread is not None:
                    self.storeThread.join()
                self.storeThread = Thread(target=self.storeBatch, args=(self.csv, Session(db.engine)))
                self.storeThread.start()
                self.newBatch()
        elif tag == "astronauts":
            self.storeBatch(self.csv, Session(db.engine))

    def characters(self, content):
        if self.currentColumn != None:
            self.row[self.currentColumn] += content
