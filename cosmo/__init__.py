from dotenv import load_dotenv
load_dotenv()

import os
from rauth import OAuth2Service
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from .config import config


db = SQLAlchemy()
oauth = None
login_manager = LoginManager()

def create_app():

    app = Flask(__name__)
    app.config.from_object(config[os.getenv("FLASK_ENV", "default")])

    db.init_app(app)
    login_manager.init_app(app)

    global oauth
    oauth = OAuth2Service(
       name = 'seznam.cz',
       client_id = app.config["OAUTH_CLIENT_ID"],
       client_secret = app.config["OAUTH_CLIENT_SECRET"],
       authorize_url = "https://login.szn.cz/api/v1/oauth/auth",
       access_token_url = "https://login.szn.cz/api/v1/oauth/token",
       base_url = "https://login.szn.cz/api/v1/"
    )
    
    from .controller_main import bp
    from .controller_rest import bp
    from .controller_auth import bp

    app.register_blueprint(controller_main.bp)
    app.register_blueprint(controller_rest.bp)
    app.register_blueprint(controller_auth.bp)

    return app
