import os

class Config:
    SQLALCHEMY_TRACK_MODIFICATIONS = False    
    JSON_AS_ASCII=False
    OAUTH_CLIENT_ID=os.getenv("OAUTH_CLIENT_ID")
    OAUTH_CLIENT_SECRET=os.getenv("OAUTH_CLIENT_SECRET")
    SQLALCHEMY_DATABASE_URI="mysql+pymysql://" + os.getenv("DB_CREDENTIALS") + "@localhost/cosmo?local_infile=1"
    ROOT_URL=os.getenv("ROOT_URL")

class DevelopmentConfig(Config):
    DEBUG = True,
    SECRET_KEY="dev"

class ProductionConfig(Config):
    DEBUG = False
    SECRET_KEY = os.getenv("SECRET_KEY")

config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig
}
