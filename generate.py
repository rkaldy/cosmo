#!/usr/bin/python3

import sys
import names
import random
from datetime import datetime

if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} row-count")
    sys.exit(1)


max_birth_date = datetime.strptime("2000-01-01", "%Y-%m-%d").timestamp()
nationalities = ("US", "GB", "CZ", "FR", "RU", None)

print("<astronauts>")
for i in range(int(sys.argv[1])):
    print("  <astronaut>")
    print("    <name>" + names.get_first_name() + "</name>")
    print("    <surname>" + names.get_last_name() + "</surname>")
    birth_timestamp = random.randint(0, max_birth_date)
    print("    <birth_date>" + datetime.fromtimestamp(birth_timestamp).strftime("%Y-%m-%d") + "</birth_date>")
    nationality = random.choice(nationalities)
    if nationality:
        print("    <nationality>" + nationality + "</nationality>")
    print("  </astronaut>")
    if i != 0 and i % 10000 == 0:
        sys.stderr.write(f"{i} rows created\n")
print("</astronauts>")
