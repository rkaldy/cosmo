DROP DATABASE IF EXISTS cosmo;
CREATE DATABASE cosmo;
USE cosmo;

CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT,INSERT,DELETE on *.* TO USER 'user'@'localhost';

CREATE TABLE astronaut (
  id int(11) NOT NULL,
  name varchar(80) NOT NULL,
  surname varchar(80) NOT NULL,
  birth_date date DEFAULT NULL,
  nationality varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

ALTER TABLE astronaut
  ADD PRIMARY KEY (id),
  ADD KEY sort (surname,name);

ALTER TABLE astronaut
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
